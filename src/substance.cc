


#include "substance.h"


Substance::Substance(int _numParticles, int _particleMass, int left, int right, int top, int bottom, sf::Color colour, int rad){
	sf::Vector2f pos, vel;
	
	numParticles = _numParticles;
	particleMass = _particleMass;
	
	radius = rad;
	
	shape.setFillColor(colour);

	shape.setRadius(radius);
	shape.setPosition(pos);
	shape.setOrigin(radius, radius);
	
	
	for (int i = 0; i < numParticles; i++) {
		pos = { (float)(randInt(left, right)), (float)(randInt(top,bottom)) };
		vel = { (float)(randInt(-VMAX,VMAX)), (float)(randInt(-VMAX, VMAX))};
		
		particles.push_back(new Particle(pos, vel, shape, particleMass));
	}
	
}


Substance::~Substance(){
	for (int i = 0; i < particles.size(); i++) {
		delete particles.at(i);
	}
}


void Substance::tick(){
	int i, k;
	
	for (i = 0; i < particles.size(); i++) {
		particles.at(i)->tick();
		
		for (k = i+1; k < particles.size(); k++){
			particles.at(i)->collide(particles.at(k));
		}
		
	}

}


inline int randInt(int min, int max){
	return (rand() % (max-min+1))+min;
}
