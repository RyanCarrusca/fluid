

#include <SFML/Graphics.hpp>
#include <algorithm>

#define WINDOW_WIDTH 1500
#define WINDOW_HEIGHT 1000

#ifndef PARTICLE_H
#define PARTICLE_H


class Particle{
	
	


public:

	static int numParticles;
	
	sf::Vector2f position;
	sf::Vector2f velocity;
	float radius;
	
	int id;


	sf::CircleShape shape;
	sf::Color color;

	double mass;

	std::vector<Particle*> recent;
	std::vector<int> recentCountdown;


	Particle(sf::Vector2f pos, sf::Vector2f vel, sf::CircleShape shap, double _mass);
	Particle(sf::Vector2f pos, sf::Vector2f vel, sf::Color colour, float rad);
	
	bool isInContact(Particle* p2);
	void collide(Particle* p2);

	void tick();

};








#endif
