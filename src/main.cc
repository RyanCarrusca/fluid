#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <time.h>
#include <stdlib.h>
#include <map>

#include "substance.h"


#define FPS_MAX 60
#define WINDOW_NAME "PV"
#define FONT_FILE "Raleway-Regular.ttf"


#define BOLTZMANN 1.38064852*pow(10,-23)
#define AVOCADO 6.022*pow(10,23)
#define R 8.3145


#define NUM_PARTICLES 300



double getTemperature(std::vector <Particle*> particles);
void collide(Particle* p1, Particle* p2);

int main() {
	srand(time((unsigned)NULL));

	sf::RenderWindow window(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), WINDOW_NAME);
	window.setFramerateLimit(FPS_MAX);

	sf::Font font;
	if (!font.loadFromFile(FONT_FILE)) {
		return EXIT_FAILURE;
	}


	Substance gas(NUM_PARTICLES, 12, 0, WINDOW_WIDTH, 0, WINDOW_HEIGHT, sf::Color::Red, 10);
	

	
	double temperature;

	sf::Text text("", font);

	text.setPosition(sf::Vector2f(0.0, 0.0));
	text.setColor(sf::Color::Black);
	text.setCharacterSize(24);


	bool start = 0;
	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event)) {
			switch (event.type) {
				case sf::Event::Closed:
					window.close();
					break;
				case sf::Event::KeyPressed:
					if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) {
						start = 1 - start;
					}
				default: {
					break;
				}
			}
		}
		if (start) {
			continue;
		}


		window.clear(sf::Color::White);
		
		gas.tick();
		for (int i=0; i < gas.particles.size(); i++){
			window.draw(gas.particles.at(i)->shape);
		}

		//std::cout << "A " << particles.at(0)->velocity.x << " " << particles.at(0)->velocity.y << std::endl;
		//std::cout << "B " << particles.at(1)->velocity.x << " " << particles.at(1)->velocity.y << std::endl;
		

		temperature = getTemperature(gas.particles);
		text.setString(std::to_string((int)temperature)+" K");
		window.draw(text);

		window.display();
	}




	return 0;
}






double getTemperature(std::vector <Particle*> particles) {
	double avSqV = 0.0;
	double avM = 0.0;


	for (int i = 0; i < particles.size(); i++) {
		avSqV += pow(particles.at(i)->velocity.x,2) + pow(particles.at(i)->velocity.y,2);
		avM += particles.at(i)->mass;
	}

	avSqV *= 1.5; //to compensate for there only being two dimensions
	avSqV /= particles.size();
	avM /= particles.size();

	return (avM * avSqV) / (3 * BOLTZMANN);
}



