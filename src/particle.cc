
#include "particle.h"



int Particle::numParticles=0;

Particle::Particle(sf::Vector2f pos, sf::Vector2f vel, sf::CircleShape shap, double _mass) {
	position = pos;
	shape = shap;
	velocity = vel;
	mass = _mass;
	
	shape.setPosition(pos);
	
	
	id = numParticles;
	numParticles++;
	
}

Particle::Particle(sf::Vector2f pos, sf::Vector2f vel, sf::Color colour, float rad) {
	position = pos;
	velocity = vel;
	radius = rad;
	mass = rad*rad*pow(10,-26);


	shape.setFillColor(colour);

	shape.setRadius(radius);
	shape.setPosition(pos);
	shape.setOrigin(radius, radius);
	
	id = numParticles;
	numParticles++;
}


//checks if the particle is touching the other particle
bool Particle::isInContact(Particle* p2) {
	for (auto i: recent){
		if (p2->id == i->id){
			return false;
		}
	}
	
	float dist = radius + p2->radius;

	float testDist = hypot(position.x - p2->position.x, position.y - p2->position.y);
	
	return ((int)testDist && (dist > testDist));
}




void Particle::collide(Particle* p2){

	double angle;
	int e, mA, mB;
	double vA1n, vA1t, vA2n, vA2t, vB1n, vB1t, vB2n, vB2t;
	double vA1x, vA1y, vA2x, vA2y, vB1x, vB1y, vB2x, vB2y;
	
	if (isInContact(p2)) {		
		angle = atan2(position.y - p2->position.y, position.x - p2->position.x);
		
		//std::cout<<"angle"<<angle<<std::endl;
		e = 1;


		mA = 2;//mass;
		mB = 2;//p2->mass;
		
		vA1x = velocity.x;
		vA1y = velocity.y;
		vB1x = p2->velocity.x;
		vB1y = p2->velocity.y;
		
		vA1n = vA1x*cos(angle) + vA1y*sin(angle);
		vA1t = vA1x*sin(angle) + vA1y*cos(angle);
		
		vB1n = vB1x*cos(angle) + vB1y*sin(angle);
		vB1t = vB1x*sin(angle) + vB1y*cos(angle);
		
		
		vA2t = vA1t;
		vB2t = vB1t;
		
		vA2n = ((mA-e*mB)*vA1n + mB*(1+e)*vB1n)/(mA+mB);
		vB2n = ((mB-e*mA)*vB1n + mA*(1+e)*vA1n)/(mB+mA);
		
		
		vA2x = vA2n*cos(angle) + vA2t*sin(angle);
		vA2y = vA2n*sin(angle) + vA2t*cos(angle);
		
		vB2x = vB2n*cos(angle) + vB2t*sin(angle);
		vB2y = vB2n*sin(angle) + vB2t*cos(angle);
		
		
		//std::cout << "A " << vA2t << " " << vA2n << " " << vA2x << " " << vA2y << std::endl;
		//std::cout << "B " << vB2t << " " << vB2n << " " << vB2x << " " << vB2y << std::endl;

		
		
		velocity.x = vA2x;
		velocity.y = vA2y;
		p2->velocity.x = vB2x;
		p2->velocity.y = vB2y;
		
		
		recent.push_back(p2);
		p2->recent.push_back(this);
		
		recentCountdown.push_back(0.5*std::min(hypot(vA2x,vA2y)*radius,hypot(vB2x,vB2y)*p2->radius));
		p2->recentCountdown.push_back(recentCountdown.at(recentCountdown.size()-1));
	}
}



void Particle::tick() {
	int index;
	for (index = recentCountdown.size()-1; index >=0; index--){
		if (recentCountdown.at(index) > 0){
			recentCountdown.at(index)--;
		}
		else{
			recentCountdown.erase(recentCountdown.begin()+index);
			recent.erase(recent.begin()+index);
		}
	} 
			

	
	
	float newX = position.x + velocity.x;
	float newY = position.y + velocity.y;

	if (newY > WINDOW_HEIGHT - radius) {
		newY = WINDOW_HEIGHT - radius;
		velocity.y *= -1;
	}
	else if (newY < radius) {
		newY = radius;
		velocity.y *= -1;
	}

	if (newX > WINDOW_WIDTH - radius) {
		newX = WINDOW_WIDTH - radius;
		velocity.x *= -1;
	}
	else if (newX < radius) {
		newX = radius;
		velocity.x *= -1;
	}

	position.x = newX;
	position.y = newY;
	shape.setPosition(position);

}


