
#include "particle.h"

#ifndef SUBSTANCE_H
#define SUBSTANCE_H


#define VMIN 1
#define VMAX 10


class Substance{
private:
	sf::CircleShape shape;


public:
	int numParticles;
	
	std::vector<Particle*> particles;
	int radius;
	int particleMass;
	
	
	Substance(int _numParticles, int _particleMass, int left, int right, int top, int bottom, sf::Color colour, int rad);
	~Substance();
	void tick();


};

inline int randInt(int min, int max);


#endif
